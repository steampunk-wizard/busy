# Integration functions must have specific names and take the command as their only argument

from busy.integration import Integration


class MockIntegration(Integration):

    def sync(self, command):
        queue = 'tasks'  # TODO: Put this in a constant somewhere
        dones = command.app.storage.get_collection(queue, 'done')
        todos = command.app.storage.get_collection(queue, 'todo')
        mylist = []
        if dones:
            mylist += [f"+ {str(d)}\n" for d in dones]
        if todos:
            mylist += [f"- {str(t)}\n" for t in todos]
        return "".join(mylist)

    def get(self, command):
        """Get a value"""
        if command.selected_indices:
            item = command.selected_items[0]
            return item.data_value('x')

    def fake(self):
        return 'j'


Main = MockIntegration
