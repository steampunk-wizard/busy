# Installation

## Briefly (MacOS)

```bash
brew install python@3.11
python3.11 -m pip install pipx
pipx install busy
```

## Long version

You'll need a terminal emulator to access a command or shell prompt. Examples include:

- iTerm2 or Terminal on MacOS
- Gnome Terminal or XTerm on Linux
- CMD on Windows
- Terminator on all platforms

Busy requires Python 3.11 and `pipx`. To check whether you already have the right version of Python on your system, start your terminal emulator and type:

```
python3 -V
```

If you don't have Python, or your version is out of date, install or upgrade it. In most cases, you'll want to do so using your system's package manager (such as Homebrew on MacOS or APT on Ubuntu). If you're not familiar with package managers, then download Python from [the Python.org site](https://www.python.org/downloads/) directly and follow the instructions provided there. When done, use the version check above to confirm it's installed and the version is 3.11 or greater.

On MacOS, this might work:

```bash
brew install python@3.11
```

On Ubuntu, this might work:

```bash
apt-get install python@3.11
```

Install `pipx` with `pip`:

```
python3.11 install pipx
pipx install busy
```

To upgrade it later:

```
pipx upgrade busy
```

## Ready to get busy?

Get all the [commands](commands.md)