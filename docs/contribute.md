# Contribute

[Request access](mailto:busy@wizlib.ca) to the [source repo](https://gitlab.com/wizlib/busy) to contribute.

The code is intended to demonstrate some Python best practices:

- *Object-oriented* with classes and subclasses.
- *Dynamic configuration* using a unique approach we call "class families" - for example, the names of the commands are properties of the command classes, not in a big "if" statement.
- *Extensive testing* with high test coverage, guaranteed by CI.
- *Leverage the standard library* by requiring as few 3rd party PIP modules as possible.

To set everything up:

- Requires Python 3.11
- Clone the repo and CD into it
- `make init`
- `poetry install`
- To run it: `python -m busy` ...

We use Visual Studio Code to build Busy, so there is a VS Code configuration file in the repository.

Then to run the test suite:

```
make test
```

Or to run test coverage:

```
make cover
```

And to check style:

```
make style
```

And to run all of the above, and prepare for CI:

```
make
```

## Ready to get Busy?

[Start over!](getting-started.md)