from datetime import date
from unittest.mock import Mock

from busy import BusyApp
from busy.command.queues_command import QueuesCommand
from busy.command.sync_command import SyncCommand
from busy.error import BusyError
from busy.model.collection.done_collection import DoneCollection
from busy.model.collection.plan_collection import PlanCollection
from busy.model.collection.todo_collection import TodoCollection
from busy.model.item import Item
from busy.storage.file_storage import FileStorage
from busy.test_case import BusyTestCase

from busy.test_case import BusyTestCase
from wizlib.parser import WizArgumentError

from test import BusyMockIntegrationTestCase


class TestCommandSync(BusyMockIntegrationTestCase):

    def test_null(self):
        a = Mock()
        a.storage = self.mock_storage(None, None, None)
        c = SyncCommand(a, integration='mock_integration')
        c.execute()
        self.assertEqual('', c.status)

    def test_with_data(self):
        o = TodoCollection([Item('a'), Item('#b')])
        d = DoneCollection(
            [Item('c', state='done', done_date=date(2023, 1, 1))])
        a = Mock()
        a.storage = self.mock_storage(o, PlanCollection(), d)
        c = SyncCommand(a, integration='mock_integration')
        p = c.execute()
        self.assertEqual('+ c\n- a\n- #b\n', p)

    def test_parse_run(self):
        o = TodoCollection([Item('a'), Item('#b')])
        d = DoneCollection(
            [Item('c', state='done', done_date=date(2023, 1, 1))])
        a = BusyApp()
        a.storage = self.mock_storage(o, PlanCollection(), d)
        with self.patchout() as o, self.patcherr():
            a.parse_run('sync', 'mock_integration')
        o.seek(0)
        self.assertIn('+ c\n- a\n- #b\n', o.read())

    def test_fails_if_no_integration(self):
        o = TodoCollection([Item('a'), Item('#b')])
        d = DoneCollection(
            [Item('c', state='done', done_date=date(2023, 1, 1))])
        a = BusyApp()
        a.storage = self.mock_storage(o, PlanCollection(), d)
        with self.patchout() as o:
            with self.assertRaises(WizArgumentError):
                a.parse_run('sync')
