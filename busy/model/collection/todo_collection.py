from . import Collection, SingleStateCollection


class TodoCollection(SingleStateCollection):

    state = 'todo'
