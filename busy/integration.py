class Integration:
    """Base class for integrations. Expected to implement sync and get
    methods."""
    pass
